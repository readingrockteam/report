<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="htsched.aspx.cs" Inherits="Hand_Tamp_Schedule_Report.htsched" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hand Tamp Schedule Report</title>
    <style type="text/css">
        #form1
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:SiteMapPath ID="SiteMapPath1" runat="server">
    </asp:SiteMapPath>
    <div style="font-size: medium; font-weight: 700">

        <CR:CrystalReportViewer ID="crvNewWebSched" runat="server" AutoDataBind="True" Height="815px"
            ReportSourceID="crsNewWebSched" Width="1200px" DisplayGroupTree="False" EnableDatabaseLogonPrompt="False"
            ReuseParameterValuesOnRefresh="True" />
        <CR:CrystalReportSource ID="crsNewWebSched" runat="server">
            <Report FileName="NewWebSched.rpt">
            </Report>
        </CR:CrystalReportSource>
    </div>
    </form>
    </body>
</html>
