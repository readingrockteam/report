﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace Reports
{
    public partial class ht_scrap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            txtDate.Text = DateTime.Now.ToShortDateString();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gvScrap.Rows)
            {
                // update verified with value in column
                SqlConnection conn = new SqlConnection("Data Source=RRS0005;Initial Catalog=Packing Solution;Persist Security Info=True;User ID=sa;Password=m2msa");
                // create a command object
                String UpdateCommand = "update scrap set uploaded = 1 where ID = " + gvScrap.DataKeys[gvr.RowIndex].Value;
                SqlCommand cmd = new SqlCommand(UpdateCommand, conn);
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    // display error

                }
                finally
                {
                    conn.Close();
                }
                // check the box
                ((CheckBox)gvr.FindControl("CheckBox1")).Checked = true;
            }
        }
    }
}
