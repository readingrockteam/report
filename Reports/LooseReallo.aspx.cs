﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Reports
{
    public partial class LooseReallo : System.Web.UI.Page
    {
        private ReportDocument report = new ReportDocument();
        protected void Page_Init(object sender, EventArgs e)
        {
            report.Load(Server.MapPath("~/Reallocation.rpt"));
            report.FileName = Server.MapPath("~/Reallocation.rpt");

            if (Session["Reallo"] != null)
            {
                ReportDocument crReportDocument = (ReportDocument)Session["Reallo"];
                crvReallo.ReportSource = crReportDocument;
            }
            else
            {
                BindReport();
            }
        }
        private void BindReport()
        {
            CrystalDecisions.Shared.TableLogOnInfo crLogonInfo;
            crLogonInfo = report.Database.Tables[0].LogOnInfo;
            crLogonInfo.ConnectionInfo.UserID = "reader";
            crLogonInfo.ConnectionInfo.Password = "reader";
            report.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
            crvReallo.DataBind();
            crvReallo.ReportSource = report;

            //Server.MapPath("~/Reallocation.rpt");
            //LooseReallo objRpt = new LooseReallo();
            ////objRpt.SetDatabaseLogon("reader", "reader");
            //crvReallo.ReportSource = objRpt;
            //Session["Reallo"] = objRpt;
        }
    }
}
