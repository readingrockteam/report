﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
namespace Reports
{
    public partial class MoldDisposeRpt : System.Web.UI.Page
    {
        private DateTime dDisposeDate;

        private ReportDocument report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Page_Init(object sender, EventArgs e)
        {

            report.Load(Server.MapPath("~/rptMoldDispose.rpt"));
            report.FileName = Server.MapPath("~/rptMoldDispose.rpt");

            if (!Page.IsPostBack)
            {
                Session["DisposeDate"] = DateTime.Parse(Request.QueryString["DisposeDate"]);
                dDisposeDate = DateTime.Parse(Session["DisposeDate"].ToString());
            }
            else
            {
                dDisposeDate = DateTime.Parse(Session["DisposeDate"].ToString());
                Session["DisposeDate"] = dDisposeDate;
            }
            BindReport();
            BindData();
        }
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            this.Unload += new EventHandler(Page_Unload);
        }
        public void Page_Unload(object sender, EventArgs e)
        {
            // clean up
            report.Clone();
            report.Dispose();
        }

        private void BindData()
        {
            report.SetParameterValue("@disposedate", dDisposeDate);

            //crvMoldDispose.ReportSource = report;

        }
        private void BindReport()
        {
            CrystalDecisions.Shared.TableLogOnInfo crLogonInfo;
            crLogonInfo = report.Database.Tables[0].LogOnInfo;
            crLogonInfo.ConnectionInfo.UserID = "reader";
            crLogonInfo.ConnectionInfo.Password = "reader";
            report.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
            crvMoldDispose.DataBind();
            crvMoldDispose.ReportSource = report;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/preMoldDispose.aspx");
        }
    }
}
