﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Hand_Tamp_Schedule_Report
{
    public partial class htsched : System.Web.UI.Page
    {
        private ReportDocument report = new ReportDocument();
        protected void Page_Init(object sender, EventArgs e)
        {
            report.Load(Server.MapPath("~/NewWebSched.rpt"));
            report.FileName = Server.MapPath("~/NewWebSched.rpt");
            if (!Page.IsPostBack)
            {
               // BindData();
            }
            BindReport();

        }
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            this.Unload += new EventHandler(Page_Unload);
        }
        public void Page_Unload(object sender, EventArgs e)
        {
            // clean up
            report.Clone();
            report.Dispose();
        }

        private void BindReport()
        {
            //Server.MapPath("~/NewWebSched.rpt");
            //Hand_Tamp_Schdule_Report.NewWebSched objRpt = new Hand_Tamp_Schdule_Report.NewWebSched();
            //objRpt.SetDatabaseLogon("reader", "reader");
            //crvNewWebSched.ReportSource = objRpt;
            //Session["htSched"] = objRpt;

            CrystalDecisions.Shared.TableLogOnInfo crLogonInfo;
            crLogonInfo = report.Database.Tables[0].LogOnInfo;
            crLogonInfo.ConnectionInfo.UserID = "reader";
            crLogonInfo.ConnectionInfo.Password = "reader";
            report.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
            crvNewWebSched.DataBind();
            crvNewWebSched.ReportSource = report;
            //Session["htSched"] = report;

        }

    }
}

