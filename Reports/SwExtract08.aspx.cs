﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Reports
{
    public partial class SwExtract08 : System.Web.UI.Page
    {
        public DataSet ds = new DataSet();
        protected void cmdLoad_Click1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("swmast");
            //SqlDataReader reader; //= new SqlDataReader();

            // create a connection object
            SqlConnection conn = new SqlConnection("Data Source=RRS0005;Initial Catalog=PS08;Persist Security Info=True;User ID=sa;Password=m2msa");

            // create a command object
            String sSelect = "select rtrim(fdrawno) + substring(fpartno, 11, len(fpartno)) piece_mark, fnotes, fquoteqty, fcuft, weight, color, release from swmast where ffmid = '" + txtSearch.Text.ToString() + "'";
            SqlCommand cmd = new SqlCommand(sSelect, conn);
            conn.Open();
            dt.Load(cmd.ExecuteReader(CommandBehavior.CloseConnection));
            ds.Tables.Add(dt);
            Server.MapPath("~/rptSwmast.rpt");
            rptSwmast objRpt = new rptSwmast();
            objRpt.SetDataSource(ds.Tables["swmast"]);

            crvSwMast.ReportSource = objRpt;
            crvSwMast.RefreshReport();
        }
        protected void cmdLoad_Click(object sender, EventArgs e)
        {
            rptSwmast obj = new rptSwmast();
            obj.SetDataSource(SqlDataSource1);
            crvSwMast.ReportSource = obj;
        }

        protected void cmdSaveVerified_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gvSwMast.Rows)
            {
                // update verified with value in column
                SqlConnection conn = new SqlConnection("Data Source=RRS0005;Initial Catalog=PS08;Persist Security Info=True;User ID=sa;Password=m2msa");
                // create a command object
                String UpdateCommand = "update swmast set verified = " + Convert.ToByte(((CheckBox)gvr.FindControl("cbxVerified")).Checked) + ", sawcut = " + Convert.ToByte(((CheckBox)gvr.FindControl("cbxSawcut")).Checked) + " where identity_column = " + gvSwMast.DataKeys[gvr.RowIndex].Value;
                SqlCommand cmd = new SqlCommand(UpdateCommand, conn);
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    // display error

                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}

