﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace Reports
{

    public class ABCPallitizeContraintsList
    {
        private List<ABCPallitizeConstraints> _List;
        public List<ABCPallitizeConstraints> List
        {
            get { return _List; }
            set { _List = value; }
        }
        public ABCPallitizeContraintsList()
        {
            _List = new List<ABCPallitizeConstraints>();
        }
        public void Add(ABCPallitizeConstraints abc)
        {
            _List.Add(abc);
        }

    }
        /*
         *  ABCPallitizeConstraints.cs
         *
         *  Created on: 03/25/2013
         *  Author: mikelj
         *
         */

    public class ABCPallitizeConstraints
    {

        // Global Variables
        private string _fac;
        public string Fac
        {
            get { return _fac; }
            set { _fac = value; }
        }

        private string _fabccode;
        public string Fabccode
        {
            get { return _fabccode; }
            set { _fabccode = value; }
        }

        private string _user_id;
        public string User_id
        {
            get { return _user_id; }
            set { _user_id = value; }
        }

        private DateTime _sys_date;
        public DateTime Sys_date
        {
            get { return _sys_date; }
            set { _sys_date = value; }
        }

        private int _identity_column;
        public int Identity_column
        {
            get { return _identity_column; }
            set { _identity_column = value; }
        }

        public void RowSet(Datasets.ABCPalletizeConstraintsSet.GetAllABCPalletizeConstraintsRow dr)
        {
            _fac = dr.fac;
            _fabccode = dr.fabccode;
            _user_id = dr.user_id;
            _sys_date = dr.sys_date;
            _identity_column = dr.identity_column;
        }

        public void New()
        {
        }
    }

    
}
