<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LooseReallo.aspx.cs" Inherits="Reports.LooseReallo" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HT Loose Inventory Reallocation Aid</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div style="height: 49px">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server">
        </asp:SiteMapPath>
        
        <br />
        REMINDER: For the most concise information, run this report after all JO's have been created according to JOMatchup suggestions.
    </div>
        <CR:CrystalReportViewer ID="crvReallo" runat="server" AutoDataBind="true" EnableDatabaseLogonPrompt="False"
            EnableParameterPrompt="False" ReportSourceID="crsReallo" Height="815px" Width="1200px"
            DisplayGroupTree="False" />
    </div>
    <CR:CrystalReportSource ID="crsReallo" runat="server">
        <Report FileName="Reallocation.rpt">
        </Report>
    </CR:CrystalReportSource>
    </form>
</body>
</html>
