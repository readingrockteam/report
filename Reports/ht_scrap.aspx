﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ht_scrap.aspx.cs" Inherits="Reports.ht_scrap" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hand Tamp Scrap</title>
    <style type="text/css">
        .style1
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" Style="font-family: Arial, Helvetica, sans-serif">
    <div>
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" >
        </asp:SiteMapPath>
        <br />
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <span class="style1">ONLY copy unchecked rows to put in MFG Report Scrap Tab.</span><br />
        <ol>
            <li>First click & drag press ctrl-c to copy and paste into a temporary excel spread sheet.</li>            
            <li>Then copy A:E.</li>            
            <li>Then Paste Special into scrap tab using "TEXT" option.</li>
            <li>Then click the update button.</li>
        </ol>
        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
            TargetControlID="txtDate">
        </cc1:CalendarExtender>
        <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" />
    </div>
    <asp:SqlDataSource ID="sqlScrap" runat="server" ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>"
        DeleteCommand="DELETE FROM [scrap] WHERE [ID] = @ID" InsertCommand="INSERT INTO [scrap] ([scrap_date], [fjobno], [fpartno], [qty], [scrap_code], [uploaded]) VALUES (@scrap_date, @fjobno, @fpartno, @qty, @scrap_code, @uploaded)"
        SelectCommand="SELECT * FROM [scrap] WHERE (datepart(yy,[scrap_date])= datepart(yy,@scrap_date) and datepart(mm,[scrap_date]) = datepart(mm, @scrap_date))"
        UpdateCommand="UPDATE [scrap] SET [uploaded] = 1 WHERE [ID] = @ID">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtDate" DbType="DateTime" Name="scrap_date" PropertyName="Text" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="uploaded" Type="Boolean" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter DbType="Date" Name="scrap_date" />
            <asp:Parameter Name="fjobno" Type="String" />
            <asp:Parameter Name="fpartno" Type="String" />
            <asp:Parameter Name="qty" Type="Int32" />
            <asp:Parameter Name="scrap_code" Type="Int32" />
            <asp:Parameter Name="uploaded" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="gvScrap" runat="server" CellPadding="4" DataSourceID="sqlScrap"
        ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataKeyNames="ID">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                SortExpression="ID" Visible="False" />
            <asp:BoundField DataField="scrap_date" DataFormatString="{0:d}" HeaderText="Scrap Date"
                SortExpression="scrap_date" />
            <asp:BoundField DataField="fjobno" HeaderText="Job No" SortExpression="fjobno" />
            <asp:BoundField DataField="fpartno" HeaderText="Part" SortExpression="fpartno" />
            <asp:BoundField DataField="qty" HeaderText="Qty" SortExpression="qty" />
            <asp:BoundField DataField="scrap_code" HeaderText="Scrap Code" SortExpression="scrap_code" />
            <asp:TemplateField HeaderText="In MFG Report" SortExpression="uploaded">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("uploaded") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("uploaded") %>' Enabled="true" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    </form>
</body>
</html>
