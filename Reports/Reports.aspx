<%@ Page Language="C#" MasterPageFile="~/RRReports.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Reports.Reports" Title="Reading Rock Reports" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="server">
    <asp:TreeView ID="TreeView1" runat="server" 
    DataSourceID="SiteMapDataSource1">
    </asp:TreeView>
    <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph2" runat="server">
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
    AutoDataBind="true" />
</asp:Content>
