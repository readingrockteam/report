﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace htWIP
{
    public partial class _Default : System.Web.UI.Page
    {
        static DataSet ds = new DataSet();
        static DataTable dtPallTran = new DataTable("pallet_tran");
        static DataTable dtWIP = new DataTable("wip");
        static DataTable dtSched = new DataTable("ht_sched");
        static DateTime dMinProdDT;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["htWIP"] != null)
            {
                ReportDocument crReportDocument = (ReportDocument)Session["htWIP"];
                crvHTWIP.ReportSource = crReportDocument;
            }
            else
            {
                //Rebind the report.
                //BindReport(dMinProdDT);
            }
        }
        protected void cmdLoad_Click(object sender, EventArgs e)
        {
            ds.Clear();
            if (ds.Tables.CanRemove(ds.Tables["wip"]))
            {
                ds.Tables.Remove("wip");
            }
            if (ds.Tables.CanRemove(ds.Tables["pallet_tran"]))
            {
                ds.Tables.Remove("pallet_tran");
            }
            if (ds.Tables.CanRemove(ds.Tables["ht_sched"]))
            {
                ds.Tables.Remove("ht_sched");
            }

            //DataTable dt = new DataTable("swmast");
            //SqlDataReader reader; //= new SqlDataReader();

            // create a connection object
            SqlConnection conn = new SqlConnection("Data Source=RRS0005;Initial Catalog=Packing Solution;Persist Security Info=True;User ID=sa;Password=m2msa");

            // create a command object
            String sSelectMinProdDT = "select min(fact_rel) fact_rel " +
                    "from m2mdata01.dbo.jomast jomast inner join ht_prod on jomast.fjobno = ht_prod.fjobno " +
                    "where jomast.fstatus = 'released'";
            SqlCommand cmd = new SqlCommand(sSelectMinProdDT, conn);

            conn.Open();
            dMinProdDT = Convert.ToDateTime(cmd.ExecuteScalar().ToString());
            String sSelectWip = "select fjobno, max(finish) as last_tamp, sum(qty_prod) as tot_prod " +
                "from ht_prod wip " +
                "where sys_date >= '" + dMinProdDT + "' " +
                "group by fjobno " +
                "order by fjobno";
            String sSelectPalletTran = "SELECT fjobno, sum(qty) tot_yarded " +
                "FROM pallet_tran " +
                "where (build_date >= '" + dMinProdDT + "') " +
                "group by fjobno order by fjobno ";
            String sSelectSched = "SELECT sorels.fdelivery, jomast.fjobno, joitem.fmqty, jomast.fjob_name, inmast.fdescript, inmast.fnusrqty1, sorels.fsono, inmast.fdrawno, soitem_ext.FEXP AS 'exp', " +
                         "soitem_ext.FXFACTOR AS 'xf', soitem_ext.FEARLY AS 'early_ship' " +
                         "FROM m2mdata01.dbo.jomast AS jomast " + 
                         "INNER JOIN m2mdata01.dbo.joitem AS joitem ON jomast.fjobno = joitem.fjobno " +
                         "LEFT OUTER JOIN m2mdata01.dbo.sorels AS sorels ON sorels.fsono = SUBSTRING(joitem.fdescmemo, 1, 6) AND sorels.finumber = SUBSTRING(joitem.fdescmemo, 8, 3) " +
                         "LEFT OUTER JOIN m2mdata01.dbo.soitem AS soitem ON sorels.fsono = soitem.fsono AND sorels.finumber = soitem.finumber " +
                         "LEFT OUTER JOIN m2mdata01.dbo.SOITEM_EXT AS soitem_ext ON soitem.identity_column = soitem_ext.FKey_ID " +
                         "LEFT OUTER JOIN m2mdata01.dbo.inmast AS inmast ON jomast.fpartno = inmast.fpartno AND inmast.fac = 'default' " +
                         "WHERE (jomast.fstatus = 'released') AND (jomast.fprodcl IN ('07', '7h', '7p', 's1', 's7'))";

            cmd.CommandText = sSelectWip;
            dtWIP.Load(cmd.ExecuteReader());
            cmd.CommandText = sSelectPalletTran;
            dtPallTran.Load(cmd.ExecuteReader());
            cmd.CommandText = sSelectSched;
            dtSched.Load(cmd.ExecuteReader());
            conn.Close();
            ds.Tables.Add(dtWIP);
            ds.Tables.Add(dtPallTran);
            ds.Tables.Add(dtSched);

            BindReport(dMinProdDT);
            crvHTWIP.Visible = true;
        }
        private void BindReport(DateTime dDate)
        {

            Server.MapPath("~/rptWIP.rpt");
            rptWIP objRpt = new rptWIP();
            objRpt.SetDataSource(ds);
            objRpt.SetDatabaseLogon("reader", "reader");
            objRpt.SetParameterValue("begin", dDate);
            //SetTableLocation(objRpt.Database.Tables);

            crvHTWIP.ReportSource = objRpt;
            Session["htWIP"] = objRpt;
            //crvHTS.RefreshReport();

            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("Sales.rpt"));


            //report.DataDefinition.FormulaFields["FiscalYear"].Text = txtFiscalYear.Text;

            //CrystalReportViewer1.ReportSource = report;
        }
        private void SetTableLocation(Tables tables)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();

            connectionInfo.ServerName = @"RRS0005";
            connectionInfo.DatabaseName = "[Packing Solution]";
            connectionInfo.UserID = "reader";
            connectionInfo.Password = "reader";
            connectionInfo.Type = ConnectionInfoType.SQL;
            //connectionInfo.IntegratedSecurity = true;


            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogOnInfo = table.LogOnInfo;
                tableLogOnInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogOnInfo);
            }
        }
    }
}
