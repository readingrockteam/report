<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="htWIP.aspx.cs" Inherits="htWIP._Default" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Hand Tamp WIP</title>
    <style type="text/css">
        .style1
        {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: medium;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" 
        style="font-family: Arial, Helvetica, sans-serif">
    </asp:SiteMapPath>
    <div>
        <span class="style1">No date to select.  Just click the load button.</span> <br />
        <asp:Button ID="cmdLoad" runat="server" Text="Load" OnClick = "cmdLoad_Click" />
        
        <CR:CrystalReportViewer ID="crvHTWIP" runat="server" AutoDataBind="false" />
    </div>
    </form>
</body>
</html>
