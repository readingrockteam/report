﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ColorCodes.aspx.cs" Inherits="Reports.ColorCodes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mismatched Color Codes and Descriptions</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" Style="font-family: Arial, Helvetica, sans-serif">
    </asp:SiteMapPath>
    <asp:Panel ID="Panel1" runat="server">
        <table>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Refresh" OnClick="Button1_Click" />
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div>
        <asp:GridView ID="gvColorCodes" runat="server" AutoGenerateColumns="False" OnPreRender="MFG_PreRender"
            CellPadding="4" DataSourceID="sqlColorCodes" ForeColor="#333333" OnRowCreated="MyGridView_RowCreated"
            GridLines="None" Style="font-family: Arial, Helvetica, sans-serif" 
            AllowSorting="True">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="color code" HeaderText="color code" SortExpression="color code" />
                <asp:BoundField DataField="color" HeaderText="color" SortExpression="color" />
                <asp:BoundField DataField="# of pieces produced" HeaderText="# of pieces produced"
                    SortExpression="# of pieces produced" />
            </Columns>
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource runat="server" ID="sqlColorCodes" ConnectionString="<%$ ConnectionStrings:Packing SolutionConnectionString %>"
        SelectCommand="SELECT color_code AS 'color code', color, num_uses AS '# of pieces produced' FROM color_codes ORDER BY 'color code', '# of pieces produced'">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlRefresh" runat="server" ConnectionString="<%$ ConnectionStrings:Packing SolutionConnectionString %>"
        SelectCommand="updateColorCodes" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </form>
</body>
</html>
