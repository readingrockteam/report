﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace crystaltest
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["byDate"] != null)
            {
                ReportDocument crReportDocument = (ReportDocument)Session["byDate"];
                crvTest.ReportSource = crReportDocument;
            }
            else
            {
                BindReport(); 
            }
        }
        private void BindReport()
        {
            Server.MapPath("~/PalletByDate.rpt");
            PalletByDate objRpt = new PalletByDate();
            objRpt.SetDatabaseLogon("reader", "reader");
            crvTest.ReportSource = objRpt;
            Session["byDate"] = objRpt;
        }

    }
}
