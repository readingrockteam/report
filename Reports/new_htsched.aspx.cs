﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Hand_Tamp_Schdule_Report
{
    public partial class _Default : System.Web.UI.Page
    {
        private DateTime dTampDate;
        private Int16 nShift;
        private String sFac;

        private ReportDocument report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Page_Init(object sender, EventArgs e)
        {

            report.Load(Server.MapPath("~/NewWebSched.rpt"));
            report.FileName = Server.MapPath("~/NewWebSched.rpt");

            if (!Page.IsPostBack)
            {
                Session["tampdate"] = DateTime.Parse(Request.QueryString["tampdate"]);
                Session["fac"] = Request.QueryString["fac"];
                Session["shift"] = Int16.Parse(Request.QueryString["shift"]);
                dTampDate = DateTime.Parse(Session["tampdate"].ToString());
                sFac = Session["fac"].ToString();
                nShift = Int16.Parse(Session["shift"].ToString());
            }
            else
            {
                dTampDate = DateTime.Parse(Session["tampdate"].ToString());
                sFac = Session["fac"].ToString();
                nShift = Int16.Parse(Session["shift"].ToString());
            }
            BindReport();
            BindData();
        }
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            this.Unload += new EventHandler(Page_Unload);
        }
        public void Page_Unload(object sender, EventArgs e)
        {
            // clean up
            report.Clone();
            report.Dispose();
        }

        private void BindData()
        {
            report.SetParameterValue("tamp_date", dTampDate);
            report.SetParameterValue("fac", sFac);
            report.SetParameterValue("shift", nShift);

            //crvNewWebSched.ReportSource = report;

        }
        private void BindReport()
        {
            CrystalDecisions.Shared.TableLogOnInfo crLogonInfo;
            crLogonInfo = report.Database.Tables[0].LogOnInfo;
            crLogonInfo.ConnectionInfo.UserID = "reader";
            crLogonInfo.ConnectionInfo.Password = "reader";
            report.Database.Tables[0].ApplyLogOnInfo(crLogonInfo);
            crvNewWebSched.DataBind();
            crvNewWebSched.ReportSource = report;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/NewHTSched.aspx");
        }

    }
}
