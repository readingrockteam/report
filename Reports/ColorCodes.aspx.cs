﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


namespace Reports
{
    public partial class ColorCodes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void MyGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == DataControlRowState.Alternate)
                {
                    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFF88';");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='White';");
                }
                else
                {
                    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFF88';");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#F7F6F3';");
                }
            }
        }
        protected void MFG_PreRender(object sender, EventArgs e)
        {
            foreach (GridViewRow grdrow in gvColorCodes.Rows)
            {
                for (int i = 0; i <= 2; i++)
                {
                    //grdrow.Cells[i].BackColor = System.Drawing.Color.GreenYellow;
                    grdrow.Cells[i].ToolTip = "see parts for color code combo: " + grdrow.Cells[0].Text + " -- " + grdrow.Cells[1].Text;
                    grdrow.Cells[i].Attributes.Add("onclick", "window.location='PartsForColor.aspx?cc=" + grdrow.Cells[0].Text + "&color=" + grdrow.Cells[1].Text + "';");
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlColorCodes.ConnectionString);
            SqlCommand cmd = new SqlCommand("updatecolorcodes", con);
            SqlCommand cmd2 = new SqlCommand(sqlColorCodes.SelectCommand, con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.ExecuteNonQuery();
            gvColorCodes.DataSourceID = null;
            gvColorCodes.DataSource = cmd2.ExecuteReader();
            gvColorCodes.DataBind();
            con.Close();            
        }
    }
}
