<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoldDisposeRpt.aspx.cs"
    Inherits="Reports.MoldDisposeRpt" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mold Dispose Report</title>
</head>
<body>
    <form id="form1" runat="server" method="get">
    <asp:Button ID="Button1" runat="server" Text="BACK" OnClick="Button1_Click" />
    <div>
        <CR:CrystalReportViewer ID="crvMoldDispose" runat="server" AutoDataBind="true" EnableParameterPrompt="False"
            EnableDatabaseLogonPrompt="False" DisplayGroupTree="False" ReuseParameterValuesOnRefresh="True" ReportSourceID="crsMoldDispose" />
        <CR:CrystalReportSource ID="crsMoldDispose" runat="server">
            <Report FileName="rptMoldDispose.rpt">
                <Parameters>
                    <CR:ControlParameter ControlID="" ConvertEmptyStringToNull="False" DefaultValue=""
                        Name="tamp_date" PropertyName="" ReportName="" />
                </Parameters>
            </Report>
        </CR:CrystalReportSource>
    </div>
    </form>
</body>
</html>
