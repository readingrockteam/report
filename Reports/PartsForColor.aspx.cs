﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Reports
{
    public partial class PartsForColor : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["cc"] = Request.QueryString["cc"];
            Session["color"] = Request.QueryString["color"];
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("colorcodes.aspx");
        }
    }
}
