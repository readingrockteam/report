<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="new_htsched.aspx.cs" Inherits="Hand_Tamp_Schdule_Report._Default" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hand Tamp Schedule Report</title>
    <style type="text/css">
        #form1
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" method="get">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Button ID="Button1" runat="server" Text="BACK" onclick="Button1_Click" />
    <div style="font-size: medium; font-weight: 700">
        <CR:CrystalReportViewer ID="crvNewWebSched" runat="server" AutoDataBind="True" Height="815px"
            ReportSourceID="crsNewWebSched" Width="1200px" DisplayGroupTree="False"
            ReuseParameterValuesOnRefresh="True" EnableParameterPrompt="False" 
            EnableDatabaseLogonPrompt="False" />
        <CR:CrystalReportSource ID="crsNewWebSched" runat="server" >
            <Report FileName="NewWebSched.rpt">
                <Parameters>
                    <CR:ControlParameter ControlID="" ConvertEmptyStringToNull="False" 
                        DefaultValue="" Name="tamp_date" PropertyName="" ReportName="" />
                    <CR:ControlParameter ControlID="" ConvertEmptyStringToNull="False" 
                        DefaultValue="" Name="shift" PropertyName="" ReportName="" />
                    <CR:ControlParameter ControlID="" ConvertEmptyStringToNull="False" 
                        DefaultValue="" Name="fac" PropertyName="" ReportName="" />
                </Parameters>
            </Report>
        </CR:CrystalReportSource>
    </div>
    </form>
</body>
</html>
