﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MismatchedMoldNum.aspx.cs"
    Inherits="Reports.WrongMold" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mismatched Mold Numbers</title>
    <style type="text/css">
        .style1
        {
            color: #FF3300;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style2
        {
            color: #00CC00;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style3
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:SiteMapPath ID="SiteMapPath1" runat="server">
    </asp:SiteMapPath>
    <asp:Panel ID="Panel1" runat="server">
        <span class="style1"><b>WARNING:</b></span><span class="style3">&nbsp; Molds listed
            in the mold column drives the demand in the mold shop.&nbsp; Therefore, the mold
            shop may not know to build a mold if it is listed here.</span><br class="style3" />
        <span class="style2"><b>NEEDED ACTIONS:</b></span><span class="style3">&nbsp; Contact
            PM to have them correct the item master in M2M(INV screen).&nbsp; Drawning number
            needs to match the mold number section of the part number.&nbsp; Released JO&#39;s
            should be reviewed to ensure the drawing matches the part on the JO.</span>
    </asp:Panel>
    <div>
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="sqlMismatchedMolds"
        ForeColor="#333333" GridLines="None" Style="font-family: Arial, Helvetica, sans-serif" PageSize="20">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="part" HeaderText="part" SortExpression="part" />
            <asp:BoundField DataField="mold" HeaderText="mold" SortExpression="mold" />
            <asp:BoundField DataField="PM" HeaderText="PM" SortExpression="PM" />
            <asp:BoundField DataField="SO" HeaderText="SO" SortExpression="SO" />
            <asp:BoundField DataField="Order Name" HeaderText="Order Name" SortExpression="Order Name" />
            <asp:BoundField DataField="job no" HeaderText="job no" ReadOnly="True" SortExpression="job no" />
            <asp:BoundField DataField="made" HeaderText="made" ReadOnly="True" SortExpression="made" />
        </Columns>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="sqlMismatchedMolds" runat="server" ConnectionString="<%$ ConnectionStrings:M2MDATA01ConnectionString %>"
        SelectCommand="spMismatchedMoldList" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    </form>
</body>
</html>
