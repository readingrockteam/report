<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PalletByDate.aspx.cs" Inherits="crystaltest._Default" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Pallet By Date</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" 
        style="font-family: Arial, Helvetica, sans-serif">
    </asp:SiteMapPath>
    <div>
        <CR:CrystalReportViewer ID="crvTest" runat="server" AutoDataBind="True" 
            EnableDatabaseLogonPrompt="False" Height="815px" 
            ReportSourceID="CrystalReportSource1" ReuseParameterValuesOnRefresh="True" 
            Width="1200px" />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
            <Report FileName="PalletByDate.rpt">
            </Report>
        </CR:CrystalReportSource>
    </div>
    </form>
</body>
</html>
