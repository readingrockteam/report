﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Reports
{
    public partial class HT_Physical : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["HTPhys"] != null)
            {
                ReportDocument crReportDocument = (ReportDocument)Session["HTPhys"];
                crvTest.ReportSource = crReportDocument;
            }
            else
            {
                BindReport();
            }
        }
        private void BindReport()
        {
            Server.MapPath("~/PalletByDate.rpt");
            HTPhysical objRpt = new HTPhysical();
            objRpt.SetDatabaseLogon("reader", "reader");
            crvTest.ReportSource = objRpt;
            Session["HTPhys"] = objRpt;
        }
    }
}
