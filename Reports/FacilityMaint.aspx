﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FacilityMaint.aspx.cs"
    Inherits="Reports.FacilityMaint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Facility Maintenance</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Style="font-family: Arial, Helvetica, sans-serif">
        </asp:SiteMapPath><br />
        <asp:GridView ID="gvXrefList" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataSourceID="sqlXrefList" ForeColor="#333333" GridLines="None">
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="M2M Facility" HeaderText="M2M Facility" 
                    SortExpression="M2M Facility" />
                <asp:BoundField DataField="Long Desc" HeaderText="Long Desc" 
                    SortExpression="Long Desc" />
                <asp:BoundField DataField="Short Desc" HeaderText="Short Desc" 
                    SortExpression="Short Desc" />
                <asp:BoundField DataField="fcfacility" HeaderText="fcfacility" 
                    SortExpression="fcfacility" />
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="sqlXrefList" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Packing SolutionConnectionString %>" 
        SelectCommand="SELECT f.fcdesc AS 'M2M Facility', x.pslongdesc AS 'Long Desc', x.psshortdesc AS 'Short Desc', f.fcfacility FROM m2mdata01.dbo.facility AS f LEFT OUTER JOIN m2mFac_ps_xref AS x ON RTRIM(f.fcfacility) = RTRIM(x.fkey_id) ORDER BY 'M2M Facility', 'Short Desc'">
    </asp:SqlDataSource>
    </form>
</body>
</html>
