﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ytd_matl_usage.aspx.cs"
    Inherits="Reports.ytd_matl_usage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>YTD Material Usage</title>
</head>
<body>
    <form id="form1" runat="server" style="font-family: Arial, Helvetica, sans-serif">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server">
    </asp:SiteMapPath>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="Label1" runat="server" Text="YTD Usage"></asp:Label><br />
    <asp:TextBox ID="txtDate" runat="server" ToolTip="Select a date in the year you are interested in"></asp:TextBox>
    <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
        TargetControlID="txtDate">
    </cc1:CalendarExtender>
    <asp:Button ID="btnPost" runat="server" Text="Update" />
    <asp:GridView ID="gvYTDUsage" runat="server" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="sqlYTDMatlUsage" ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="fpartno" HeaderText="Material" SortExpression="fpartno" />
            <asp:BoundField DataField="fmeasure" HeaderText="UofM" SortExpression="fmeasure" />
            <asp:BoundField DataField="fcost" HeaderText="Costs" SortExpression="fcost" />
            <asp:BoundField DataField="usage" HeaderText="Usage" ReadOnly="True" SortExpression="usage"
                DataFormatString="{0:n}" />
            <asp:BoundField DataField="usage_tons" DataFormatString="{0:n}" HeaderText="Tons"
                SortExpression="usage_tons" />
            <asp:BoundField DataField="ext_cost" DataFormatString="{0:c}" HeaderText="Extended Costs"
                ReadOnly="True" SortExpression="ext_cost" />
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <div>
    </div>
    <asp:SqlDataSource ID="sqlYTDMatlUsage" runat="server" ConnectionString="<%$ ConnectionStrings:M2MDATA01ConnectionString %>"
        SelectCommand="SELECT intran.fpartno, inmast.fmeasure, intran.fcost, SUM(intran.fqty) * - 1 AS usage, CASE inmast.fmeasure WHEN 'lbs' THEN (SUM(intran.fqty) * - 1) / 2000 ELSE SUM(intran.fqty) * - 1 END AS usage_tons, intran.fcost * SUM(intran.fqty) * - 1 AS ext_cost FROM intran AS intran INNER JOIN inmast ON inmast.fac = intran.fac AND inmast.fpartno = intran.fpartno AND inmast.frev = intran.fcpartrev WHERE (DATEPART(yy, intran.fdate) = DATEPART(yy, @date)) AND (intran.ftojob LIKE 'I%') AND (intran.ftype = 'I') GROUP BY intran.fpartno, inmast.fmeasure, intran.fcost ORDER BY intran.fpartno">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtDate" Name="date" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
