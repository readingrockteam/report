﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenSOs.aspx.cs" Inherits="Reports.OpenSOs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Open SOs need JOs</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:SiteMapPath ID="SiteMapPath1" runat="server">
    </asp:SiteMapPath>
    <div>
        The oldest Date Ordered is used to know how much JO history is needed to gather
        to successfully create the correct JO&#39;s.&nbsp;&nbsp; Reasons that an old SO
        might be listed here are:&nbsp; Pallet may have been damaged and new parts are needed.&nbsp;
        Customer may have called to say they needed more parts.&nbsp; If the SO does not
        need to be open, close it.&nbsp; The older the order date, the longer it takes the
        JO Matchup process to run.<br />
    </div>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="sqlSOITEM" ForeColor="#333333"
        GridLines="None" PagerSettings-PageButtonCount="10" PageSize="20" PagerStyle-Font-Size="X-Small">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="SO" HeaderText="SO" SortExpression="SO" />
            <asp:BoundField DataField="Line" HeaderText="Line" SortExpression="Line" />
            <asp:BoundField DataField="Part" HeaderText="Part" SortExpression="Part" />
            <asp:BoundField DataField="Ord Qty" HeaderText="Ord Qty" 
                SortExpression="Ord Qty" />
            <asp:BoundField DataField="Order Name" HeaderText="Order Name" 
                SortExpression="Order Name" />
            <asp:BoundField DataField="Status" HeaderText="Status" 
                SortExpression="Status" />
            <asp:BoundField DataField="Due Date" HeaderText="Due Date" ReadOnly="True" 
                SortExpression="Due Date" />
            <asp:BoundField DataField="Open Demand" HeaderText="Open Demand" 
                SortExpression="Open Demand" />
            <asp:BoundField DataField="Qty Made" HeaderText="Qty Made" 
                SortExpression="Qty Made" />
            <asp:BoundField DataField="Qty On Hand" HeaderText="Qty On Hand" 
                SortExpression="Qty On Hand" />
            <asp:BoundField DataField="Date Ordered" HeaderText="Date Ordered" 
                ReadOnly="True" SortExpression="Date Ordered" />
            <asp:BoundField DataField="Coordinator" HeaderText="Coordinator" 
                SortExpression="Coordinator" />
            <asp:BoundField DataField="Facility" HeaderText="Facility" 
                SortExpression="Facility" />
            <asp:BoundField DataField="SO key for JO" HeaderText="SO key for JO" 
                ReadOnly="True" SortExpression="SO key for JO" />
            <asp:BoundField DataField="Source Fac" HeaderText="Source Fac" 
                SortExpression="Source Fac" />
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="sqlSOITEM" runat="server" ConnectionString="<%$ ConnectionStrings:M2MDATA01ConnectionString %>"
        
        SelectCommand="/* SOITEM */ SELECT soitem.fsono AS 'SO', soitem.finumber AS 'Line', soitem.fpartno AS 'Part', soitem.fquantity AS 'Ord Qty', somast.fordername AS 'Order Name', somast.fstatus AS 'Status', CONVERT (varchar(10), somast.fduedate, 111) AS 'Due Date', inmast.fbook AS 'Open Demand', inmast.fproqty AS 'Qty Made', inmast.fonhand AS 'Qty On Hand', CONVERT (varchar(10), somast.forderdate, 111) AS 'Date Ordered', somast.fsocoord AS 'Coordinator', soitem.fac AS 'Facility', soitem.fsono + soitem.finumber + '000' AS 'SO key for JO', soitem.sfac AS 'Source Fac' FROM soitem INNER JOIN somast ON soitem.fsono = somast.fsono INNER JOIN sorels ON soitem.finumber = sorels.finumber AND soitem.fsono = sorels.fsono INNER JOIN inmast ON soitem.fpartno = inmast.fpartno AND inmast.fonhand + inmast.fproqty - inmast.fbook &lt; 0 AND inmast.fdrawno NOT LIKE 'sale%' AND inmast.fabccode &lt;&gt; 'v' WHERE (somast.fstatus IN ('OPEN', 'ON HOLD')) AND (inmast.fprodcl IN (SELECT DISTINCT fprodcl FROM RRPC WHERE (jo_cell = 'ht'))) AND (SUBSTRING(soitem.fpartno, 5, 2) &lt;&gt; 'XX') AND (inmast.fac IN (SELECT DISTINCT fkey_id FROM [Packing Solution].dbo.m2mFac_ps_xref)) ORDER BY somast.forderdate">
    </asp:SqlDataSource>
    </form>
</body>
</html>
