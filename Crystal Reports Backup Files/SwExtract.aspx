<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SwExtract.aspx.cs" Inherits="Solidworks_Extraction._Default" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Solidworks Extraction Report</title>
    <style type="text/css">
        .style1
        {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-style: italic;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" Style="font-family: Arial, Helvetica, sans-serif">
    </asp:SiteMapPath>
    <div>
        <asp:Panel ID="Panel1" runat="server">
            <span class="style1">Enter the Filemaker ID:</span>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2"
                DataTextField="release" DataValueField="release">
            </asp:DropDownList>
            <asp:Button ID="cmdLoad" runat="server" Text="LOAD" />
            <asp:Button ID="cmdSaveVerified" runat="server" OnClick="cmdSaveVerified_Click" Text="Verified" />
        </asp:Panel>
    </div>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>"
        SelectCommand="select distinct release from swmast where ffmid = @ffmid">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtSearch" Name="ffmid" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="gvSwMast" runat="server" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Style="font-family: Arial, Helvetica, sans-serif"
        PageSize="20" AllowSorting="True" DataKeyNames="Identity_Column">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:BoundField DataField="Piece Mark" HeaderText="Piece Mark" SortExpression="Piece Mark"
                ReadOnly="True" />
            <asp:TemplateField HeaderText="verified" SortExpression="verified">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("verified") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cbxVerified" runat="server" Checked='<%# Bind("verified") %>' Enabled="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="sawcut" SortExpression="sawcut">
                <EditItemTemplate>
                    <asp:CheckBox ID="checkbox2" runat="server" Checked='<%# Bind("sawcut") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cbxSawcut" runat="server" Checked='<%# Bind("sawcut") %>' Enabled="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Part" HeaderText="Part" SortExpression="Part" />
            <asp:BoundField DataField="Notes" HeaderText="Notes" SortExpression="Notes" />
            <asp:BoundField DataField="Qty" HeaderText="Qty" SortExpression="Qty" />
            <asp:BoundField DataField="CUFT" HeaderText="CUFT" SortExpression="CUFT" />
            <asp:BoundField DataField="Wgt" HeaderText="Wgt" SortExpression="Wgt"></asp:BoundField>
            <asp:BoundField DataField="color" HeaderText="color" SortExpression="color" />
            <asp:BoundField DataField="release" HeaderText="release" SortExpression="release" />
            <asp:BoundField DataField="Path" HeaderText="Path" SortExpression="Path" />
            <asp:BoundField DataField="Verify" HeaderText="Verify" ReadOnly="True" SortExpression="Verify"
                Visible="False" />
            <asp:BoundField DataField="ffmid" HeaderText="ffmid" SortExpression="ffmid" Visible="False" />
            <asp:BoundField DataField="Identity_Column" HeaderText="Identity_Column" InsertVisible="False"
                ReadOnly="True" SortExpression="Identity_Column" Visible="False" />
        </Columns>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>"
        SelectCommand="SELECT RTRIM(fdrawno) + SUBSTRING(fpartno, 11, LEN(fpartno)) AS 'Piece Mark', fpartno AS Part, fnotes AS Notes, fquoteqty AS Qty, fcuft AS CUFT, weight AS Wgt, color, release, extract_path AS Path, 0 AS Verify, ffmid, Identity_Column, ISNULL(verified, 0) AS verified, ISNULL(sawcut, 0) AS sawcut FROM swmast WHERE (ffmid = @ffmid) AND (release = @release) ORDER BY fdrawno"
        
        DeleteCommand="delete from swmast where identity_column = @Identity_column" 
        UpdateCommand="UPDATE swmast SET verified = @verified, sawcut = @sawcut WHERE (Identity_Column = @identity_column)">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtSearch" Name="ffmid" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="DropDownList1" Name="release" PropertyName="SelectedValue" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="Identity_column" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="verified" />
            <asp:Parameter Name="sawcut" />
            <asp:Parameter Name="identity_column" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <CR:CrystalReportViewer ID="crvSwMast" runat="server" AutoDataBind="false" />
    </form>
</body>
</html>
