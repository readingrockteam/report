﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewHTSched.aspx.cs" Inherits="Reports.NewHTSched" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New HT Schedule</title>
    <style type="text/css">
        #form1
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:SiteMapPath ID="SiteMapPath1" runat="server">
    </asp:SiteMapPath>
    <div style="font-size: medium; font-weight: 700">
        <asp:Panel ID="Panel1" runat="server" Height="36px">
            <table style="width: 100%;">
                <tr>
                    <td>
                        Tamp Date:
                        <asp:TextBox ID="txtTampDate" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:ImageButton runat="server" ID="CalendarButton" ImageUrl="~/images/calendarbutton.png"
                            AlternateText="Click here to display calendar" />
                        <cc1:CalendarExtender ID="txtTampDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtTampDate" PopupButtonID="CalendarButton">
                        </cc1:CalendarExtender>
                    </td>
                    <td>
                        Facility:
                        <asp:DropDownList ID="ddlFac" runat="server" DataSourceID="dsPackingSolution" 
                            AutoPostBack="True" DataTextField="fac" DataValueField="fac">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Shift:
                        <asp:DropDownList ID="ddlShift" runat="server" DataSourceID="dsShift" DataTextField="shift"
                            DataValueField="shift" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnRun" runat="server" Text="Run Report" OnClick="Btn_DisplayReport" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <asp:SqlDataSource ID="dsPackingSolution" runat="server" ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>"
        SelectCommand="select distinct fac from ht_sched where tamp_date = @tamp_date order by fac">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtTampDate" Name="tamp_date" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsShift" runat="server" ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>"
        SelectCommand="select distinct shift from ht_sched where tamp_date = @tamp_date and fac = @fac order by shift">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtTampDate" Name="tamp_date" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlFac" Name="fac" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
