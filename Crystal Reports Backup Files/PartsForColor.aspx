﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartsForColor.aspx.cs"
    Inherits="Reports.PartsForColor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Parts List By Color</title>
    <style type="text/css">
        .style1
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" method="get" >

    <asp:Panel ID="Panel1" runat="server" Height="77px">
        <span class="style1">
        <asp:Button ID="btnBack" runat="server" onclick="btnBack_Click" Text="BACK" />
        </span><br class="style1" />
        <span class="style1">Look up these parts in M2M(INV screen) and obsolete them. 
        You will have to close, cancel or delete any JO&#39;s for these parts frist.</span><br 
            class="style1" />  <span class="style1">If there is demand for the part, 
        then there is an OPEN sales order that will need the part removed as well.
        </span> <br />
    </asp:Panel>
    <div>
        <asp:GridView ID="gvParts" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataSourceID="sqlParts" ForeColor="#333333" GridLines="None" 
            style="font-family: Arial, Helvetica, sans-serif">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="Part" HeaderText="Part" SortExpression="Part" />
                <asp:BoundField DataField="demand" HeaderText="demand" 
                    SortExpression="demand" />
                <asp:BoundField DataField="job no" HeaderText="job no" ReadOnly="True" 
                    SortExpression="job no" />
                <asp:BoundField DataField="job name" HeaderText="job name" ReadOnly="True" 
                    SortExpression="job name" />
                <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" 
                    SortExpression="Status" />
            </Columns>
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="sqlParts" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Packing SolutionConnectionString %>" 
        SelectCommand="SELECT i.fpartno AS Part, i.fbook AS demand, ISNULL(j.fjobno, '') AS 'job no', ISNULL(j.fjob_name, '') AS 'job name', ISNULL(j.fstatus, '') AS 'Status' FROM m2mdata01.dbo.inmast AS i LEFT OUTER JOIN m2mdata01.dbo.jomast AS j ON i.fpartno = j.fpartno AND i.fac = j.fac WHERE (SUBSTRING(i.fpartno, 5, 2) = @cc) AND (SUBSTRING(i.fmusrmemo1, 1, LEN(@color)) = @color) and i.fcstscode = 'a'">
        <SelectParameters>
            <asp:SessionParameter Name="cc" SessionField="cc" />
            <asp:SessionParameter Name="color" SessionField="color" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
    </body>
</html>
