﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="preMoldDispose.aspx.cs" Inherits="Reports.MoldDispose" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mold Disposed List</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 <div>
        <div style="height: 30px">
            <asp:SiteMapPath ID="SiteMapPath1" runat="server">
            </asp:SiteMapPath>
        </div>
       <div style="font-size: medium; font-weight: 700">
        <asp:Panel ID="Panel1" runat="server" Height="36px">
            <table style="width: 100%;">
                <tr>
                    <td>
                        select any day within the month to run the report for:
                        <asp:TextBox ID="txtDisposeDate" runat="server" AutoPostBack="True"></asp:TextBox>
                        <asp:ImageButton runat="server" ID="CalendarButton" ImageUrl="~/images/calendarbutton.png"
                            AlternateText="Click here to display calendar" />
                        <cc1:CalendarExtender ID="txtDisposeDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtDisposeDate" PopupButtonID="CalendarButton">
                        </cc1:CalendarExtender>
                    </td>
                    <td>
                        <asp:Button ID="btnRun" runat="server" Text="Run Report" OnClick="Btn_DisplayReport" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
 </div>
    </form>
</body>
</html>
