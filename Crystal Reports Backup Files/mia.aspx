﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mia.aspx.cs" Inherits="Reports.mia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hand Tamp MIA Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Arial, Helvetica, sans-serif">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server">
        </asp:SiteMapPath>
        <br />
        MIA List
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" CellPadding="4" DataSourceID="sqlMIA"
            ForeColor="#333333" GridLines="None">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="sqlMIA" runat="server" ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>"
        SelectCommand="MIA" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </form>
</body>
</html>
