﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="printer_xref.aspx.cs" Inherits="Reports.printer_xref" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Printer Xref Maint</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Font-Names="Arial">
        </asp:SiteMapPath>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataKeyNames="fac,cell,user_id" DataSourceID="sqlPrinterXref" 
            ForeColor="#333333" GridLines="None">
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                    ShowSelectButton="True" />
                <asp:BoundField DataField="fac" HeaderText="fac" ReadOnly="True" 
                    SortExpression="fac" />
                <asp:BoundField DataField="cell" HeaderText="cell" ReadOnly="True" 
                    SortExpression="cell" />
                <asp:BoundField DataField="user_id" HeaderText="user_id" ReadOnly="True" 
                    SortExpression="user_id" />
                <asp:BoundField DataField="packing_label" HeaderText="packing_label" 
                    SortExpression="packing_label" />
                <asp:BoundField DataField="packing_list" HeaderText="packing_list" 
                    SortExpression="packing_list" />
                <asp:BoundField DataField="packing_id" HeaderText="packing_id" 
                    SortExpression="packing_id" />
                <asp:BoundField DataField="sys_date" HeaderText="sys_date" 
                    SortExpression="sys_date" />
                <asp:BoundField DataField="identity_column" HeaderText="identity_column" 
                    InsertVisible="False" ReadOnly="True" SortExpression="identity_column" />
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="sqlPrinterXref" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Packing SolutionConnectionString %>" 
        DeleteCommand="DELETE FROM [printer_xref] WHERE [fac] = @fac AND [cell] = @cell AND [user_id] = @user_id" 
        InsertCommand="INSERT INTO [printer_xref] ([fac], [cell], [user_id], [packing_label], [packing_list], [packing_id], [sys_date]) VALUES (@fac, @cell, @user_id, @packing_label, @packing_list, @packing_id, @sys_date)" 
        SelectCommand="SELECT * FROM [printer_xref]" 
        UpdateCommand="UPDATE [printer_xref] SET [packing_label] = @packing_label, [packing_list] = @packing_list, [packing_id] = @packing_id, [sys_date] = @sys_date WHERE [fac] = @fac AND [cell] = @cell AND [user_id] = @user_id">
        <DeleteParameters>
            <asp:Parameter Name="fac" Type="String" />
            <asp:Parameter Name="cell" Type="String" />
            <asp:Parameter Name="user_id" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="packing_label" Type="String" />
            <asp:Parameter Name="packing_list" Type="String" />
            <asp:Parameter Name="packing_id" Type="String" />
            <asp:Parameter DbType="DateTime" Name="sys_date" />
            <asp:Parameter Name="identity_column" Type="Int32" />
            <asp:Parameter Name="fac" Type="String" />
            <asp:Parameter Name="cell" Type="String" />
            <asp:Parameter Name="user_id" Type="String" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="fac" Type="String" />
            <asp:Parameter Name="cell" Type="String" />
            <asp:Parameter Name="user_id" Type="String" />
            <asp:Parameter Name="packing_label" Type="String" />
            <asp:Parameter Name="packing_list" Type="String" />
            <asp:Parameter Name="packing_id" Type="String" />
            <asp:Parameter DbType="DateTime" Name="sys_date" />
        </InsertParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
