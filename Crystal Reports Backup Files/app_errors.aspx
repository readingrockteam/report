﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="app_errors.aspx.cs" Inherits="Reports.app_errors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Application Errors</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" Font-Names="Arial">
    </asp:SiteMapPath>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataKeyNames="identity_column" DataSourceID="sqlPackingSolution" 
            ForeColor="#333333" GridLines="None" PageSize="20">
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:BoundField DataField="identity_column" HeaderText="identity_column" 
                    InsertVisible="False" ReadOnly="True" SortExpression="identity_column" 
                    Visible="False" />
                <asp:BoundField DataField="fac" HeaderText="fac" SortExpression="fac" />
                <asp:BoundField DataField="application" HeaderText="application" 
                    SortExpression="application" />
                <asp:BoundField DataField="catch_error" HeaderText="catch_error" 
                    SortExpression="catch_error" />
                <asp:BoundField DataField="user_id" HeaderText="user_id" 
                    SortExpression="user_id" />
                <asp:BoundField DataField="sys_date" HeaderText="sys_date" 
                    SortExpression="sys_date" />
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="sqlPackingSolution" runat="server" 
        ConnectionString="<%$ ConnectionStrings:Packing_SolutionConnectionString %>" 
        SelectCommand="SELECT * FROM [app_errors] ORDER BY [sys_date] DESC"></asp:SqlDataSource>
    </form>
</body>
</html>
